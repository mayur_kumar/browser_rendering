# Browser Rendering

<!-- vscode-markdown-toc -->
* 1. [Introduction](#Introduction)
* 2. [Parsing](#Parsing)
	* 2.1. [Construction of Dom](#ConstructionofDom)
	* 2.2. [Construction of CSSOM](#ConstructionofCSSOM)
* 3. [The render tree](#Therendertree)
* 4. [Layouting of the render tree](#Layoutingoftherendertree)
* 5. [Painting the render tree](#Paintingtherendertree)
* 6. [Adding Interactivity with JavaScript](#AddingInteractivitywithJavaScript)
* 7. [References](#References)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Introduction'></a>Introduction

A browser is simply software that loads different files and displays them to users and allows users to interact. Every browser has a browser engine
for eg., Chrome has a v8 engine.
The browser reads the raw bytes of data, and not the actual characters in the files of HTML, CSS, and javaScript.The browser receives the bytes of data, but it can’t do anything with it. The raw bytes of data must be converted to a form that it can understand. The browsers read the files and the browser engine processes the data under certain processes to make the content display on the page

![Intro](https://3fxtqy18kygf3on3bu39kh93-wpengine.netdna-ssl.com/wp-content/uploads/2019/11/Screenshot-2019-11-12-at-3.26.19-PM.png)
            

##  2. <a name='Parsing'></a>Parsing

###  2.1. <a name='ConstructionofDom'></a>Construction of Dom

While reading the HTML file, when Browser gets an encounter with elements like ``html``, ``body``, ``div``, etc., it converts them into a JavaScript object called a node. Thus all the Html elements are converted into nodes. These nodes are further linked together to form a tree-like structure called DOM. The relation between every node is established in the form of Parent-Child relationship, adjacent sibling relation,etc.

_The DOM contains all the information about the page’s HTML element’s relationships_

![DOM illustration](https://miro.medium.com/max/520/1*YSA8lCfCVPn3d6GWAVokrA.png)


###  2.2. <a name='ConstructionofCSSOM'></a>Construction of CSSOM

While reading the HTML files, if the browser encounters a link tag to the CSS file it requests to fetch the CSS files, and the CSS object model is created. The formation of CSSOM is similar to that of DOM

Firstly, the CSS bytes are converted into characters, then tokens, then nodes, and finally they are linked into a tree structure known as the "__CSS Object Model" (CSSOM)__

Each node in this tree contains style information of DOM elements that it targets with the help of selectors. CSSOM, however, does not contain DOM elements that can’t be printed on the screen like ``<meta>``, ``<script>``,``<title>``, etc.

_CSSOM contains information on how the HTML elements are styled_

![cssom picture](https://miro.medium.com/max/357/1*DJg1yRx-AzkZposWbJKcaA.png)


##  3. <a name='Therendertree'></a>The render tree

The browser combines both DOM and CSSOM to form a render tree. The render tree only has information that can be visible. For example, if the node has CSS property like _"display: none"_ it will be not included in the render tree because it is hidden by a CSS property, and only nodes that can be displayed on the page are only included in the rende tree.

> DOM + CSSOM = Render tree

_The render tree contains only the nodes required to render the page._

![render tree](https://miro.medium.com/max/1000/1*8HnhiojSoPaJAWkruPhDwA.png)

##  4. <a name='Layoutingoftherendertree'></a>Layouting of the render tree

When a render tree is created, the position and size values are not assigned. The Browser calculates and figures out the exact position and size of each element that should be displayed on the screen with the help of the information in the render tree

Finally, every node is assigned the exact coordinates. This ensures that every node appears at an accurate position on the screen.

##  5. <a name='Paintingtherendertree'></a>Painting the render tree

With the information from DOM, CSSOM, and the exact layout of the elements computed, the browser now “paints” the individual node to the screen. Finally, the elements are now rendered to the screen.

This step is often referred to as __painting__,__rasterizing__ or __repainting__.

##  6. <a name='AddingInteractivitywithJavaScript'></a>Adding Interactivity with JavaScript

JavaScript can alter both DOM and CSSOM according to the user interaction. We can modify content by adding and removing elements from the DOM tree, we can modify the CSSOM properties of each element, handle user input, and much more with the help of javascript.

While reading the HTML file, whenever the browser encounters a script tag, the DOM construction is paused. 

The entire DOM construction process is stopped until the script finishes executing.This is the reason ``<script>`` tag is not placed at the top in HTML document 

## 7. <a name='References'></a> References
[Understanding DOM, CSSOM, Render Tree, Layout, and Painting](https://medium.com/weekly-webtips/understand-dom-cssom-render-tree-layout-and-painting-9f002f43d1aa)

[How browser rendering works — behind the scenes](https://blog.logrocket.com/how-browser-rendering-works-behind-scenes/)

[How Browsers Work: Behind the scenes of modern web browsers](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/#The_HTML_grammar_definition)

[How the browser renders a web page? — DOM, CSSOM, and Rendering](https://medium.com/jspoint/how-the-browser-renders-a-web-page-dom-cssom-and-rendering-df10531c9969)
